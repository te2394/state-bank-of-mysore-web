import { combineReducers } from 'redux';

import ccounterReducer from './ccounterReducer';
import custReducer from './custReducer';

const crootReducer = combineReducers({
    ccounterReducer, custReducer
});

export default crootReducer;