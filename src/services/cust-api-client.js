const url = process.env.REACT_APP_GCP_CUST_API_URL;
const custAPIClient = {
    getAllCust: function () {
        var promise = new Promise((resolve, reject) => {
            return fetch(url+ "/tickets").then((res) => {
                var result = res.json();
                result.then((jResult) => {
                    resolve(jResult);    console.log(jResult);
                }, (err) => {
                    reject("JSON Parsee Error");
                });
            }).catch((err) => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    insertCust: function (p) {
        const request = new Request(url, {
            method: 'POST',
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(p)
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("JSON Parse Error");
                })
            }).catch(error => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    updateCust: function (p) {
        const request = new Request(url + "/" + p.ticketId, {
            method: 'PUT',
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(p)
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then((jResult) => {
                    resolve(jResult);
                }, (err) => {
                    reject("JSON Parse Error");
                })
            }).catch(error => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    deleteCust: function (p) {
        const request = new Request(url + "/" + p.ticketId, {
            method: 'DELETE'
        });

        var promise = new Promise((resolve, reject) => {
            return fetch(request).then(res => {
                res.json().then(() => {
                    resolve("Record Deleted");
                }, (err) => {
                    reject("JSON Parse Error");
                })
            }).catch(error => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    },

    fetchTicketsBycustomerId: function (p) {   console.log("fetchTicketsBycustomerId api..");
        var promise = new Promise((resolve, reject) => {
            //return fetch(url+ "/customer/" + p.custId).then((res) => {
                return fetch(url+ "/" + p.custId).then((res) => {
                var result = res.json();
                result.then((jResult) => {
                    resolve(jResult);    console.log(jResult);
                }, (err) => {
                    reject("JSON Parsee Error");
                });
            }).catch((err) => {
                reject("Error connecting to the API");
            });
        });

        return promise;
    }
}

export default custAPIClient;