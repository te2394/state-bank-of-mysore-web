// npm i reselect

import { createSelector } from 'reselect';

const getCust = (state, props) => state.custReducer.cust;
const getticketId = (state, props) => props.match.params.ticketId;

const getCustById = createSelector(getCust, getticketId, function (cust, ticketId) {
    const custt = cust.find(p => p.ticketId === parseInt(ticketId));
    return custt;
});

export default getCustById;