// npm i reselect

import { createSelector } from 'reselect';

const getCust = (state, props) => state.custReducer.cust;
const getcustId = (state, props) => props.match.params.custId;

const getTicketsCustById = createSelector(getCust, getcustId, function (cust, custId) {
    const custt = cust.find(p => p.custId === parseInt(custId));
    return custt;
});

export default getTicketsCustById;