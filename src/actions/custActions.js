import * as actionTypes from './actionTypes';

import custAPIClient from '../services/cust-api-client';
import history from '../utilities/history';

function loadCustRequested(msg) {
    return {
        type: actionTypes.LOAD_CUST_REQUESTED,
        payload: { message: msg, flag: false }
    };
}

function loadCustSuccess(cust, msg) {
    return {
        type: actionTypes.LOAD_CUST_SUCCESS,
        payload: { data: cust, message: msg, flag: true }
    };
}

function loadCustFailed(msg) {
    return {
        type: actionTypes.LOAD_CUST_FAILED,
        payload: { message: msg, flag: true }
    };
}

export function loadCust() {
    return function (dispatch) {
        dispatch(loadCustRequested("Customer Request Started..."));

        custAPIClient.getAllCust().then(cust => {
            setTimeout(() => {
                dispatch(loadCustSuccess(cust, "Customer Request Completed..."));
            }, 5000);
        }).catch(eMsg => {
            dispatch(loadCustFailed(eMsg));
        });
    }
}
function loadCustTicketRequested(msg) {
    return {
        type: actionTypes.LOAD_CUSTTICKET_REQUESTED,
        payload: { message: msg, flag: false }
    };
}

function loadCustTicketSuccess(cust, msg) {
    return {
        type: actionTypes.LOAD_CUSTTICKET_SUCCESS,
        payload: { data: cust, message: msg, flag: true }
    };
}

function loadCustTicketFailed(msg) {
    return {
        type: actionTypes.LOAD_CUSTTICKET_FAILED,
        payload: { message: msg, flag: true }
    };
}

export function loadTicketsBycustomerId(cust) {  console.log("loadTicketsBycustomerId");
    return function (dispatch) {
        dispatch(loadCustTicketRequested("CustTicket Request Started..."));

        custAPIClient.fetchTicketsBycustomerId(cust).then(cust => {
            setTimeout(() => {
                dispatch(loadCustTicketSuccess(cust, "Customer Ticket Request Completed..."));
            }, 5000);
        }).catch(eMsg => {
            dispatch(loadCustTicketFailed(eMsg));
        });
    }
}

// ----------------------------------------------------------- INSERT

function insertCustSuccess(cust, msg) {
    return {
        type: actionTypes.INSERT_CUST_SUCCESS,
        payload: { data: cust, message: msg, flag: true }
    };
}

export function insertCust(cust) {  console.log("actn insert");
    return function (dispatch) {
        custAPIClient.insertCust(cust).then(insertCust => {
            dispatch(insertCustSuccess(insertCust, "Customer Inserted Successfully..."));
            history.push('/custs');
        }).catch(eMsg => {
            console.error(eMsg);
        });
    }
}

// ----------------------------------------------------------- UPDATE

function updateCustSuccess(cust, msg) {
    return {
        type: actionTypes.UPDATE_CUST_SUCCESS,
        payload: { data: cust, message: msg, flag: true }
    };
}

export function updateCust(cust) { console.log("actn update");
    return function (dispatch) {
        custAPIClient.updateCust(cust).then(updateCust => {
            dispatch(updateCustSuccess(updateCust, "Customer Updated Successfully..."));
            history.push('/custs');
        }).catch(eMsg => {
            console.error(eMsg);
        });
    }
}

// ----------------------------------------------------------- DELETE

function deleteCustSuccess(cust, msg) {
    return {
        type: actionTypes.DELETE_CUST_SUCCESS,
        payload: { data: cust, message: msg, flag: true }
    };
}

export function deleteCust(cust) {
    return function (dispatch) {
        custAPIClient.deleteCust(cust).then(_ => {
            dispatch(deleteCustSuccess(cust, "Customer Deleted Successfully..."));
        }).catch(eMsg => {
            console.error(eMsg);
        });
    }
}