// import 'bootstrap/dist/css/bootstrap.css';
// import 'bootstrap-icons/font/bootstrap-icons.css';
// import './index.css';

// import React from 'react';
// import ReactDOM from 'react-dom';
// import 'bootstrap';

// import RootComponent from './components/root/RootComponent';
// import configureStore from './store/configureStore';
// import * as counterActions from './actions/counterActions';

// ReactDOM.render(
//   <React.StrictMode>
//     <RootComponent />
//   </React.StrictMode>,
//   document.getElementById('root')
// );

// // const appStore = configureStore();
// // console.log(appStore);
// // console.log(appStore.getState());

// // const appStore = configureStore({ counterReducer: 100 });
// // console.log(appStore);
// // console.log(appStore.getState());

// const appStore = configureStore();
// console.log("Loaded State", appStore.getState());

// appStore.subscribe(() => {
//   console.log("Store State Changed");
//   console.log("New State is: ", appStore.getState());
// });

// // Below code will be used in Container Components
// // Take Actions from ActionCreators
// let incAction = counterActions.incCounter();
// let decAction = counterActions.decCounter();

// appStore.dispatch(incAction);
// appStore.dispatch(incAction);

// appStore.dispatch(decAction);
// appStore.dispatch(decAction);

// --------------------------------------------------------------------
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-icons/font/bootstrap-icons.css';
import './index.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'bootstrap';

import RootComponent from './components/root/RootComponent';
import configureStore from './store/configureStore';

const appStore = configureStore();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={appStore}>
      <RootComponent />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);