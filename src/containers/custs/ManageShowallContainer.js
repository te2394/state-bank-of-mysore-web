import React, { Component } from 'react';
import { connect } from 'react-redux';
import ShowallComponent from '../../components/custs/ShowallComponent';
import getTicketsCustById from '../../selectors/getTicketsCustById';
import * as custActions from '../../actions/custActions';
import ShowallListComponent from '../../components/custs/ShowallListComponent';

class ManageShowallContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cust: { ...this.props.cust }
        };
        console.log("ManageShowallContainer");
        
        this.updateState = this.updateState.bind(this);
       // this.saveCust = this.saveCust.bind(this);
       this.showallTickets = this.showallTickets.bind(this);
    }

    updateState(e) {
        
        console.log("updateState");
        const field = e.target.name;
        console.log(field);
        let cust = { ...this.state.cust };
        console.log( e.target.value);
        cust[field] = e.target.value;
        console.log( cust );
        this.setState({ cust: cust });
    }


    showallTickets(e){
        e.preventDefault();
        if (this.state.cust.custId) {
            console.log("showallTickets");
            this.props.loadTicketsBycustomerId(this.state.cust);

        } else {
            console.log("showallTickets else");
        }
    }

    render() {
       
        return (
            
            <div>
               
                <ShowallComponent pageText={this.props.pText} cust={this.state.cust}
                    onChange={this.updateState} onSave={this.showallTickets} />
               
                </div>
               
            
           
        );
    
    }
}

function mapStateToProps(state, ownProps) {
    console.log("SHmapStateToProps");
    const custId = ownProps.match.params.custId;

    let cust = {
        ticketId: "",
        custId: "",
        email: "",
        message: "",
        phoneNumber: "",
        statusType: ""
    };

    if (custId && state.custReducer.cust.length > 0) {  console.log("state.custReducer.cust.length > 0");
        cust = getTicketsCustById(state, ownProps);
    }

    var pText = cust.custId === "" ? "Create Customer" : "Edit Customer";
    console.log(cust);
    return {
        pText, cust
    };
}

function mapDispatchToProps(dispatch) {
    console.log("SHmapDispatchToProps");
    
    return {
        loadTicketsBycustomerId: (cust) => { dispatch(custActions.loadTicketsBycustomerId(cust)); }
        
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageShowallContainer);