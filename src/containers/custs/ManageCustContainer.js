import React, { Component } from 'react';
import { connect } from 'react-redux';
import CustFormComponent from '../../components/custs/CustFormComponent';
import getCustById from '../../selectors/getCustById';

import * as custActions from '../../actions/custActions';

class ManageCustContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cust: { ...this.props.cust }
        };
        console.log("MAnageCustContainer");
        
        this.updateState = this.updateState.bind(this);
        this.saveCust = this.saveCust.bind(this);
    }

    updateState(e) {
        
        console.log("updateState");
        const field = e.target.name;
        console.log(field);
        let cust = { ...this.state.cust };
        console.log( e.target.value);
        cust[field] = e.target.value;
        console.log( cust );
        this.setState({ cust: cust });
    }

    saveCust(e) {
        e.preventDefault();
        console.log("saveCust");
        console.log( this ); 
        console.log( this.state);
        console.log( this.state.cust );
        if (this.state.cust.ticketId) {
            console.log("ifupdate");
            this.props.updateCust(this.state.cust);
        } else {
            console.log("elseinsert");
            this.props.insertCust(this.state.cust);
        }

        // this.props.history.push('/products');
    }

    render() {
        return (
            <div>
                <CustFormComponent pageText={this.props.pText} cust={this.state.cust}
                    onChange={this.updateState} onSave={this.saveCust} />
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    console.log("MMmapStateToProps");
    const ticketId = ownProps.match.params.ticketId;

    let cust = {
        ticketId: "",
        custId: "",
        email: "",
        message: "",
        phoneNumber: "",
        statusType: ""
    };

    if (ticketId && state.custReducer.cust.length > 0) {
        cust = getCustById(state, ownProps);
    }

    var pText = cust.ticketId === "" ? "Create Customer" : "Edit Customer";

    return {
        pText, cust
    };
}

function mapDispatchToProps(dispatch) {
    console.log("MMmapDispatchToProps");
    return {
        insertCust: (cust) => { dispatch(custActions.insertCust(cust)); },
        updateCust: (cust) => { dispatch(custActions.updateCust(cust)); },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageCustContainer);