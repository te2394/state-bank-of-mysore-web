import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as custActions from '../../actions/custActions';
import LoaderAnimation from '../../components/common/LoaderAnimation';
import AddCustButton from '../../components/custs/AddCustButton';
import CustListComponent from '../../components/custs/CustListComponent';
import ShowAll  from '../../components/custs/ShowAll';
class CustsContainer extends Component {
    render() { console.log("CustsContainer");
        return (
            <>   
                {
                  
                    this.props.flag ?
                        <>
                            <div className='mt-3 mb-3'>
                                <AddCustButton />  <ShowAll />
                            </div>
                         
                            <CustListComponent cust={this.props.cust} onDelete={
                                (p, e) => {
                                    this.props.deleteCust(p);
                                }
                            } />
                        </>
                        : <LoaderAnimation />
                }
            </>
        );
    }

    componentDidMount() {
        console.log("componentDidMount");
        console.log(this.props);
        this.props.loadCust();
    }
}

function mapStateToProps(state, ownProps) {
    console.log("mapStateToProps"+state.custReducer.cust);
    return {
        cust : state.custReducer.cust,
        status: state.custReducer.status,
        flag: state.custReducer.flag
    };
}

function mapDispatchToProps(dispatch) {
    console.log("mapDispatchToProps"+dispatch);
    return {
        loadCust: () => { dispatch(custActions.loadCust()); },
        deleteCust: (cust) => { dispatch(custActions.deleteCust(cust)); }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustsContainer);