import { combineReducers } from 'redux';

import counterReducer from './counterReducer';
import productReducer from './productReducer';
//import ccounterReducer from './ccounterReducer';
import custReducer from './custReducer';

const rootReducer = combineReducers({
    //counterReducer, productReducer, ccounterReducer,custReducer
    counterReducer, productReducer,custReducer
});

export default rootReducer;