// Reducer is a Pure Function
// A reducer is a function that determines changes to an application's state.
// It uses the action it receives via dispatcher, to determine this change. 

import * as actionTypes from '../actions/actionTypes';
import cinitialState from "./cinitialState";

const custReducer = (state = cinitialState.custData, action) => {
    switch (action.type) {
       
        case actionTypes.LOAD_CUST_REQUESTED:
        case actionTypes.LOAD_CUST_FAILED:
            console.log(222222222222);
            return {
                cust: [...state.cust],
                status: action.payload.message,
                flag: action.payload.flag
            };
        // case actionTypes.LOAD_CUST_SUCCESS:
        //     console.log(3333333333333);
        //     return {
        //         cust: [...action.payload.data],
        //         status: action.payload.message,
        //         flag: action.payload.flag
        //     };            
        case actionTypes.LOAD_CUST_SUCCESS:
            console.log(3333333333333);
            console.log(" case actionTypes.LOAD_CUST_SUCCESS");
            console.log(Object.values(action.payload.data));
            return {
                cust: Object.values(action.payload.data),
                status: action.payload.message,
                flag: action.payload.flag
            };
        case actionTypes.INSERT_CUST_SUCCESS:
            console.log(44444444444444);
            return {
                cust: [...state.cust, { ...action.payload.data }],
                status: action.payload.message,
                flag: action.payload.flag
            };
        case actionTypes.UPDATE_CUST_SUCCESS:
            console.log(555555555555);
            var itemIndex = state.cust.findIndex(p => p.ticketId === parseInt(action.payload.data.ticketId));
            var tempProducts = [...state.cust];
            tempProducts.splice(itemIndex, 1, { ...action.payload.data });
            return {
                cust: [...tempProducts],
                status: action.payload.message,
                flag: action.payload.flag
            };
        case actionTypes.DELETE_CUST_SUCCESS:
            console.log(666666666666);
            return {
                cust: [...state.cust.filter(p => p.ticketId !== parseInt(action.payload.data.ticketId))],
                status: action.payload.message,
                flag: action.payload.flag
            };
        default:
            return state;
    }
}

export default custReducer;