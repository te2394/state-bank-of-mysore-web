import React from 'react';
import { NavLink } from 'react-router-dom';

import SwitchComponent from '../../routes';

import './NavigationComponent.css';

var logo = require('../../logo.svg').default;

const NavigationComponent = () => {
    return (
        <>
            <nav className="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
                <div className="container-fluid">
                    <NavLink className="navbar-brand d-flex flex-column align-items-center" to="/">
                        <img src={logo} alt="React" width="40" height="28" className="d-inline-block align-text-top" />
                       State Bank Of Mysore
                    </NavLink>

                    <button type="button" className="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#myNavbar">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="myNavbar">
                        <ul className="navbar-nav ms-auto">
                            <li className="nav-item px-3">
                                <NavLink exact className="nav-link d-flex flex-column align-items-center" to="/">
                                    <i className="bi bi-house-fill"></i>
                                    <span>Home</span>
                                </NavLink>
                            </li>
                            {/* <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/about">
                                    <i className="bi bi-file-person-fill"></i>
                                    <span>About</span>
                                </NavLink>
                            </li> */}
                            {/* <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/hoc1">
                                    <i className="bi bi-dice-1-fill"></i>
                                    <span>HOC One</span>
                                </NavLink>
                            </li>
                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/hoc2">
                                    <i className="bi bi-dice-2-fill"></i>
                                    <span>HOC Two</span>
                                </NavLink>
                            </li>
                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/counter">
                                    <i className="bi bi-123"></i>
                                    <span>Counter</span>
                                </NavLink>
                            </li> */}
                            {/* <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/products">
                                    <i className="bi bi-box"></i>
                                    <span>Products</span>
                                </NavLink>
                            </li> */}
                            {/* <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/cust">
                                    <i className="bi bi-box"></i>
                                    <span>Customer</span>
                                </NavLink>
                            </li> */}
                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/#">
                                    <i className="bi bi-box"></i>
                                    <span>Accounts</span>
                                </NavLink>
                            </li>   
                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/#">
                                    <i className="bi bi-box"></i>
                                    <span>Loans</span>
                                </NavLink>
                            </li>
                                                                                   
                            <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/custs">
                                    <i className="bi bi-box"></i>
                                    <span>Customer</span>
                                </NavLink>
                            </li>
                            {/* <li className="nav-item px-3">
                                <NavLink className="nav-link d-flex flex-column align-items-center" to="/login">
                                    <i className="bi bi-person-square"></i>
                                    <span>Login</span>
                                </NavLink>
                            </li> */}

                            {/* <li className="nav-item px-3">
                                <NavLink className="nav-link logout d-flex flex-column align-items-center" to="/login">
                                    <i className="bi bi-person-square"></i>
                                    <span>Logout</span>
                                </NavLink>
                            </li> */}
                        </ul>
                    </div>
                </div>
            </nav>

            <>
                {SwitchComponent}
            </>
        </>
    );
};

export default NavigationComponent;