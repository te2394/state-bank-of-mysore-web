import React from 'react';
import { useState } from 'react';
import { Link } from 'react-router-dom';

import ConfirmModal from '../common/ConfirmModal';

const CustListComponent = ({ cust, onDelete }) => {
    return (
        <table className="table table-hover">
            <thead>
                <tr>
                    <th>Ticket Id</th>
                    <th>Cust Id</th>
                    <th>Email</th>
                    <th>Message</th>
                    <th>PhoneNumber</th>
                    <th>Status</th>
                    {/* <th>&nbsp;</th>
                    <th>&nbsp;</th> */}
                </tr>
            </thead>
            <tbody>
                {
                    cust.map(cust => <CustListRow key={cust.ticketId} cust={cust} onDelete={onDelete} />)
                }
            </tbody>
        </table>
    );
};

const CustListRow = ({ cust, onDelete }) => {
    var [show, setShow] = useState(false);

    return (
        <>
            <tr key={cust.ticketId}>
                <td>{cust.ticketId}</td>
                <td>{cust.custId}</td>
                <td>{cust.email}</td>
                <td>{cust.message}</td>
                <td>{cust.phoneNumber}</td>
                <td>{cust.statusType}</td>
                {/* <td>
                    <Link className="text-info" to={"cust/" + cust.ticketId}>Edit</Link>
                </td>
                <td>
                    <Link className="text-danger" to={"cust/" + cust.ticketId} onClick={
                        e => {
                            e.preventDefault();
                            setShow(true);
                        }
                    }>Delete</Link>
                </td> */}
            </tr>

            <ConfirmModal show={show} handleYes={e => {
                onDelete(cust, e);
                setShow(false);
            }} handleNo={e => {
                setShow(false);
            }} />
        </>
    );
};

export default CustListComponent;