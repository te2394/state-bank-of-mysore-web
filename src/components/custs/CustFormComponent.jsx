import React from 'react';
import { Link } from 'react-router-dom';

import TextInput from '../common/TextInput';

const CustFormComponent = ({ pageText, cust, onChange, onSave }) => {
    return (
        <>
            <h1 className="text-info text-center">{pageText}</h1>
            <div className="text-center">
                <Link to="/custs">Back to List</Link>
            </div>

            <div className="row">
                <div className="col-sm-6 offset-sm-3">
                    <form className="justify-content-center" onSubmit={onSave}>
                        <fieldset>
                            <legend className="text-center">Enter Customer Information</legend>

                            <TextInput name="ticketId" label="Ticket Id" value={cust.ticketId} readOnly={true}/>
                            <TextInput name="custId" label="Cust ID" value={cust.custId} onChange={onChange} />
                            <TextInput name="email" label="Email" value={cust.email} onChange={onChange} />
                            <TextInput name="message" label="Message" value={cust.message} onChange={onChange} />
                            <TextInput name="phoneNumber" label="Phone Number" value={cust.phoneNumber} onChange={onChange} />
                            <TextInput name="statusType" label="Status" value={cust.statusType} onChange={onChange} />
                        
                            <div className="d-grid gap-2 mx-auto col mt-3">
                                <button type='submit' className="btn btn-success">Save</button>
                                <button type='button' className="btn btn-secondary">Reset</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </>
    );
};

export default CustFormComponent;