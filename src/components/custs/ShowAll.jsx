import React from 'react';
import { withRouter } from 'react-router-dom';

const ShowAll = ({ history }) => {
    return (
        <button className='btn btn-primary' onClick={
            (e) => {
                history.push('/showall');
            }
        }>
            Show All
        </button>
    );
};

export default withRouter(ShowAll);