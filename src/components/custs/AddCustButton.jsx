import React from 'react';
import { withRouter } from 'react-router-dom';

const AddCustButton = ({ history }) => {
    return (
        <button className='btn btn-primary' onClick={
            (e) => {
                history.push('/cust');
            }
        }>
            Add Custmer
        </button>
    );
};

export default withRouter(AddCustButton);