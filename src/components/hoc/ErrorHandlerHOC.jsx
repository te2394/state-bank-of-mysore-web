import { Component } from "react";

const ErrorHandlerHOC = (InputComponent) => class extends Component {
    static get displayName() {
        return "ErrorHandlerHOC";
    }

    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    static getDerivedStateFromError(error) {
        return { hasError: true };
    }

    render() {
        if (this.state.hasError) {
            var cError = require('../../assets/component-error.png');
            return (
                <div className="text-center mt-5">
                    <h2 className="text-danger mb-5">Error Loading Component</h2>
                    <img src={cError} alt="Component Error" className="img-fluid" />
                </div>
            ); 
        }
        else
            return <InputComponent {...this.state} {...this.props} />;
    }
}

export default ErrorHandlerHOC;